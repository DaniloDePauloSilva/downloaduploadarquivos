package controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import io.FileSaver;

@Controller
public class ControllerPrincipal {
	
	
	@RequestMapping("/")
	public ModelAndView index()
	{
		ModelAndView mv = new ModelAndView("principal");
		
		
		return mv;
	}
	
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	@ResponseBody
	public String uploadArquivo(MultipartFile arquivo) 
	{
		try 
		{
			System.out.println("MULTIPART FILE: ");
			System.out.println(arquivo);
			String path = new FileSaver().write("arquivos", arquivo);
			
			System.out.println(path);
			
			return "{ \"resposta\": \"OK\" }";	
		}
		catch(Exception ex) 
		{
			return "{ \"resposta\": \"ERRO\" }";
		}
		
	}
	
	@RequestMapping(value="/download", method=RequestMethod.GET)
	public void downloadFile(HttpServletResponse response) throws Exception
	{
		File file = new File("C:/Users/danilo.silva/ARQUIVOS_UPLOAD/CORREÇÕES BANCO.csv");
		
		String mimeType = URLConnection.guessContentTypeFromName(file.getName());
		
		if(mimeType == null)
		{
			System.out.println("mimetype is not detectable, will take default");
			mimeType = "application/octet-stream";
		}
		
		System.out.println("mimetype : "+mimeType);
	  
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() +"\""));
		response.setContentLength((int)file.length());
		InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
	}
	

}
