package io;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileSaver {
	
	public String write(String diretorio, MultipartFile file)
	{
		try 
		{
			String realPath = "C:/Users/danilo.silva/ARQUIVOS_UPLOAD";
			String caminho = realPath + "/" + file.getOriginalFilename();
			file.transferTo(new File(caminho));
			
			return diretorio + "/" + file.getOriginalFilename();

		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}

}
